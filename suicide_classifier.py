# -*- coding: utf-8 -*-
"""suicide_classifier"""

!unzip "glove.6B.100d.txt.zip"

from __future__ import print_function

from keras.models import Model
from keras.layers import Input, LSTM, Dense
import numpy as np

batch_size = 64  # Batch size for training.
epochs = 10  # Number of epochs to train for.
latent_dim = 256  # Latent dimensionality of the encoding space.
num_samples = 10000  # Number of samples to train on.
fname="500_Reddit_users_posts_labels.csv"

import pandas as pd

import os

data=pd.read_csv(fname)

data.head(10)

embeddings_index = {}
f = open(os.path.join('glove.6B.100d.txt'))
for line in f:
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f.close()

import re

# ques_list=list()
# ans_list=list()
all_sentences=list()
# senLength=25
for index, row in data.iterrows():
    raw_sentence=row['Post']
    sentence = raw_sentence.lower()
    sentence = re.sub("[.]+" , " .", sentence)
    sentence = re.sub( '[^a-zA-Z0-9.\']', ' ', sentence )
    sentence=re.sub("\s+" , " ", sentence)
    sentences=sentence.split("\'")
    new_sen=[]
    for s in sentences:
      if len(s)>1:
        sentence=s.split(".")
        clean_sens=[]
        for sen in sentence:
          words=sen.split(" ")
          clean_sen=[]
          for word in words:
             if word in embeddings_index:
               clean_sen.append(word)
          if len(clean_sen)>1:
            clean_sen=" ".join(clean_sen)   

            clean_sens.append(clean_sen)
    all_sentences.append(clean_sens)

all_sentences[:2]

len(all_sentences)

# maxLen=-1
for div in all_sentences:
  for s in div:
    if len(s)==maxLen:
      print(s)
      break
      # maxLen=max(len(s),maxLen)

fname1="train_dataset.xlsx"

data1=pd.read_excel(fname1)

data1.head(10)

from sklearn.preprocessing import OneHotEncoder
encoder = OneHotEncoder(sparse=False)

# sui_data=list(data1[data1["Suicidal"]=="oui"]["Tweets"])

sui_data_X=data1["Tweets"].values

labels = data1["Suicidal"].values

len(sui_data_X),len(labels)

import string  
puncs=string.punctuation

def clean_sentence(sentence):
  sentence = sentence.lower()
  sentence = re.sub( "[^a-zA-Z0-9\']", ' ', sentence )
  # sentence=re.sub("\s+" , " ", sentence)
  words = sentence.split()
  new_words=[ w for w in words if w not in puncs  ]
  sen=" ".join(new_words)
  return sen.strip()

update_sentences=[]
update_labels=[]
for s,label in zip(sui_data_X,labels):
  s=clean_sentence(s)
  update_labels.append(label)
  update_sentences.append(s)

update_sentences=np.array(update_sentences)
update_labels=np.array(update_labels)

labels=update_labels.reshape((-1, 1))
labels=encoder.fit_transform(labels)

data2=pd.DataFrame({'sentences':update_sentences,'labels':update_labels })

data2.head(10)

len(labels),len(update_sentences)

from sklearn.model_selection import train_test_split

sentences_train, sentences_test, y_train, y_test = train_test_split(update_sentences, labels, test_size=0.10, random_state=1000)

from keras.preprocessing.text import Tokenizer

tokenizer = Tokenizer(num_words=20000, oov_token='<unw>')

tokenizer.fit_on_texts(sentences_train)

X_train = tokenizer.texts_to_sequences(sentences_train)
X_test = tokenizer.texts_to_sequences(sentences_test)

from keras.preprocessing.sequence import pad_sequences

maxlen = 200

X_train = pad_sequences(X_train, padding='post', maxlen=maxlen)

X_test = pad_sequences(X_test, padding='post', maxlen=maxlen)

max_features=20000

word_index = tokenizer.word_index

num_words = min(max_features, len(word_index)) + 1
print(num_words)

embedding_dim=100

embeddings_index = {}
f = open('glove.6B.100d.txt')
for line in f:
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f.close()

print('Found %s word vectors.' % len(embeddings_index))

# first create a matrix of zeros, this is our embedding matrix
embedding_matrix = np.zeros((num_words, embedding_dim))

# for each word in out tokenizer lets try to find that work in our w2v model
for word, i in word_index.items():
    if i > max_features:
        continue
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        # we found the word - add that words vector to the matrix
        embedding_matrix[i] = embedding_vector
    else:
        # doesn't exist, assign a random vector
        embedding_matrix[i] = np.random.randn(embedding_dim)
f.close()

from keras.models import Sequential
from keras import layers

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import *
from keras.utils.np_utils import to_categorical
from keras.initializers import Constant

model = Sequential()
model.add(Embedding(num_words,
                    embedding_dim,
                    embeddings_initializer=Constant(embedding_matrix),
                    input_length=maxlen,
                    trainable=True))
model.add(SpatialDropout1D(0.2))
model.add(Bidirectional(CuDNNLSTM(64, return_sequences=True)))
model.add(Bidirectional(CuDNNLSTM(32)))
model.add(Dropout(0.25))
model.add(Dense(units=2, activation='softmax'))
model.compile(loss = 'categorical_crossentropy', optimizer='adam',metrics = ['accuracy'])
print(model.summary())

from keras.utils import plot_model
plot_model(model, to_file='model.png')

history = model.fit(X_train, y_train,
                    epochs=50,
                    verbose=True,
                    validation_data=(X_test, y_test),
                    batch_size=10)

# plot_history(history)

loss, accuracy = model.evaluate(X_train, y_train, verbose=False)
print("Training Accuracy: {:.4f}".format(accuracy))
loss, accuracy = model.evaluate(X_test, y_test, verbose=False)
print("Testing Accuracy:  {:.4f}".format(accuracy))

model.save("suicide_classifier.h5")

sui_data=list(data1[data1["Suicidal"]=="oui"]["Tweets"])
non_sui_data=list(data1[data1["Suicidal"]=="non"]["Tweets"])

sui_data[:5]

train = tokenizer.texts_to_sequences(sui_data[:5])

train

tokenizer.index_word[27]

