from flask import Flask, request,render_template,jsonify
import numpy as np
import pickle,re,string  
from scipy import spatial
import keras
puncs=string.punctuation

def clean_sentence(sentence):
  sentence = sentence.lower()
  sentence = re.sub( "[^a-zA-Z\']", ' ', sentence )
  sentence= re.sub("\s+" , " ", sentence)
  words = sentence.split(' ')
  new_words=[ w for w in words if w not in puncs  ]
  return new_words

def get_sentence_embedding(q):
  em=np.zeros((100,))
  q=clean_sentence(q)
  for w in q:
    if w:
      w = re.sub( '[^a-zA-Z]', '', w )
      w=w.lower()
      if w in embeddings_index:
        em+=embeddings_index[w]
  return em  	

app = Flask(__name__)
GLOVE_EMBEDDING_SIZE = 100
embeddings_index = pickle.load( open( "embeddings_index.p", "rb" ) )
embedding_score_ques = pickle.load( open( "embedding_score_ques_updated.p", "rb" ) )
ans_list = pickle.load( open( "ans_list_updated.p", "rb" ) )

def getMaxGroup(test_text):
  test_em=get_sentence_embedding(test_text)
  maxScore=-100000000000
  maxGroup=-1
  for idx,score_group in enumerate(embedding_score_ques):
    for score in score_group:
      similarity=1 - spatial.distance.cosine(list(score), list(test_em))
      if similarity > maxScore:
        maxScore= similarity
        maxGroup=idx
  ans_texts=".".join(ans_list[maxGroup])
  return ans_texts.strip() 

@app.route('/')
def index():
	return render_template("index.html")

@app.route('/bot-response')
def update():
	msg= request.args.get('msg', 0, type=str)
	result=getMaxGroup(msg)
	return jsonify(result=result)

if __name__ == '__main__': 
	app.run(debug=False)