# -*- coding: utf-8 -*-
"""seq2seq_prediction"""

from keras.models import Model
from keras.layers.recurrent import LSTM
from keras.layers import *
from keras.preprocessing.sequence import pad_sequences
from keras.callbacks import ModelCheckpoint
from collections import Counter
import nltk
import numpy as np
from sklearn.model_selection import train_test_split
import urllib.request
import os
import sys
import zipfile

np.random.seed(42)

BATCH_SIZE = 64
NUM_EPOCHS = 100
GLOVE_EMBEDDING_SIZE = 100
HIDDEN_UNITS = 256
MAX_INPUT_SEQ_LENGTH = 100
MAX_TARGET_SEQ_LENGTH = 100
MAX_VOCAB_SIZE = 5000

!unzip "glove.6B.100d.txt.zip"

def load_glove():
    _word2em = {}
    file = open("glove.6B.100d.txt", mode='rt', encoding='utf8')
    for line in file:
        words = line.strip().split()
        word = words[0]
        embeds = np.array(words[1:], dtype=np.float32)
        _word2em[word] = embeds
    file.close()
    return _word2em

embeddings_index = load_glove()

fname="500_Reddit_users_posts_labels.csv"

import pandas as pd
data=pd.read_csv(fname)

import re

# ques_list=list()
# ans_list=list()
all_sentences=list()
# senLength=25
for index, row in data.iterrows():
    raw_sentence=row['Post']
    sentence = raw_sentence.lower()
    sentence = re.sub("[.]+" , " .", sentence)
    sentence = re.sub( '[^a-zA-Z.\']', ' ', sentence )
    sentence=re.sub("\s+" , " ", sentence)
    sentences=sentence.split("\'")
    new_sen=[]
    for s in sentences:
      if len(s)>1:
        sentence=s.split(".")
        clean_sens=[]
        for sen in sentence:
          words=sen.split(" ")
          clean_sen=[]
          for word in words:
             if word in embeddings_index:
               clean_sen.append(word)
          if len(clean_sen)>1:
            clean_sen=" ".join(clean_sen)   

            clean_sens.append(clean_sen)
    all_sentences.append(clean_sens)

ques_list=[]
ans_list=[]
target_counter = Counter()

max_seq_length=50

all_sentences_merged=[]
for div in all_sentences:
  for i,s in enumerate(div):
    if i>0:
      if len(pre_sen)>max_seq_length:
        ques_list.append(pre_sen[:max_seq_length])
      else:
        ques_list.append(pre_sen)

      if len(s)>max_seq_length:
        ans_list.append('<start> ' + s[:max_seq_length] + ' <end>')
      else:
        ans_list.append('<start> ' + s +' <end>')
    pre_sen=s
    s='<start> ' + s +' <end>'
    all_sentences_merged.append(s)

fname1="train_dataset.xlsx"

data1=pd.read_excel(fname1)

sui_data_X=list(data1["Tweets"].values)

sui_data_X[:5]

import string  
puncs=string.punctuation

def clean_sentence(sentence):
  sentence = sentence.lower()
  sentence = re.sub( "[^a-zA-Z\']", ' ', sentence )
  # sentence=re.sub("\s+" , " ", sentence)
  words = sentence.split()
  new_words=[ w for w in words if w not in puncs  ]
  sen=" ".join(new_words)
  return sen.strip()

for idx,s in enumerate(sui_data_X):
  sui_data_X[idx]=clean_sentence(s)

sui_data_X[:5]

all_sentences_merged+=sui_data_X

len(all_sentences_merged)

import pickle

pickle.dump( all_sentences_merged, open( "all_sentences_merged_50.p", "wb" ) )

for s in all_sentences_merged[:5]:
  print( s.split(" "))    
    # for w in s.split(" "):
      # target_counter[w] += 1

target_counter = Counter()

for s in all_sentences_merged:
  # print( s.split(" "))    
    for w in s.split(" "):
      target_counter[w] += 1

pickle.dump( target_counter, open( "target_counter_actual_50.p", "wb" ) )

ques_list[0]

pickle.dump( ques_list, open( "ques_list_50.p", "wb" ) )
pickle.dump( ans_list, open( "ans_list_50.p", "wb" ) )

for idx,(ques,ans) in enumerate(zip(ques_list,ans_list)):
  ques_list[idx]=ques.split(" ")
  ans_list[idx]=ans.split(" ")

ques_list[0],ans_list[0]

pickle.dump( ques_list, open( "ques_list_word_splitted_50.p", "wb" ) )
pickle.dump( ans_list, open( "ans_list_word_splitted_50.p", "wb" ) )

MAX_VOCAB_SIZE=5000

word2idx = dict()
for idx, word in enumerate(target_counter.most_common(MAX_VOCAB_SIZE)):
    word2idx[word[0]] = idx + 1

if '<unk>' not in word2idx:
    word2idx['<unk>'] = 0

len(word2idx)

idx2word = dict([(idx, word) for word, idx in word2idx.items()])

pickle.dump( word2idx, open( "word2idx_50.p", "wb" ) )
pickle.dump( idx2word, open( "idx2word_50.p", "wb" ) )

num_tokens = len(idx2word)+1

num_tokens

def encode_words(words_list):
  encoded_ques=[]
  for w in words_list:
    if w in word2idx:
      encoded_ques.append(word2idx[w])
    else:
      encoded_ques.append(word2idx['<unk>'])
  return encoded_ques

encoded_ques_list=[]
encoded_ans_list=[]
for input_words, target_words in zip(ques_list, ans_list):
    encoded_ques_list.append(encode_words(input_words))
    encoded_ans_list.append(encode_words(target_words))

encoded_ques_list[0]

pickle.dump( encoded_ques_list, open( "encoded_ques_list_50.p", "wb" ) )
pickle.dump( encoded_ans_list, open( "encoded_ans_list_50.p", "wb" ) )

encoded_ques_list = pad_sequences(encoded_ques_list, maxlen=max_seq_length, padding='post')
encoded_ans_list = pad_sequences(encoded_ans_list, maxlen=max_seq_length, padding='post')

encoded_ques_list[0]

Xtrain, Xtest, Ytrain, Ytest = train_test_split(encoded_ques_list, encoded_ans_list, test_size=0.2, random_state=42)

# one hot encode target sequence
def encode_output(sequences, vocab_size):
  ylist = list()
  for sequence in sequences:
    encoded = to_categorical(sequence, num_classes=vocab_size)
    ylist.append(encoded)
  y = np.array(ylist)
  y = y.reshape(sequences.shape[0], sequences.shape[1], vocab_size)
  return y

from keras.utils import to_categorical
from keras.utils.vis_utils import plot_model
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense
from keras.layers import Embedding
from keras.layers import RepeatVector
from keras.layers import TimeDistributed
from keras.callbacks import ModelCheckpoint

Ytrain = encode_output(Ytrain,num_tokens )
Ytest = encode_output(Ytest,num_tokens )

del Ytrain,Ytest

pickle.dump( Xtrain , open( "Xtrain_qa_50.p", "wb" ) )
pickle.dump( Xtest, open( "Xtest_qa_50.p", "wb" ) )
pickle.dump( Ytrain, open( "Ytrain_qa_50.p", "wb" ) )
pickle.dump( Ytest, open( "Ytest_qa_50.p", "wb" ) )

# first create a matrix of zeros, this is our embedding matrix
embedding_matrix = np.zeros((num_tokens, GLOVE_EMBEDDING_SIZE))

# for each word in out tokenizer lets try to find that work in our w2v model
for word, i in word2idx.items():
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        # we found the word - add that words vector to the matrix
        embedding_matrix[i] = embedding_vector
    else:
        # doesn't exist, assign a random vector
        embedding_matrix[i] = np.random.randn(GLOVE_EMBEDDING_SIZE)

from keras.initializers import Constant

def define_model():
  model = Sequential()
  model.add(Embedding(num_tokens, HIDDEN_UNITS, input_length=max_seq_length, mask_zero=True,
                      embeddings_initializer=Constant(embedding_matrix),trainable=False))
  model.add(Bidirectional(LSTM(HIDDEN_UNITS, return_sequences=True)))
  model.add(Bidirectional(LSTM(HIDDEN_UNITS)))
  # model.add(LSTM(HIDDEN_UNITS))
  model.add(RepeatVector(max_seq_length))
  model.add(LSTM(HIDDEN_UNITS, return_sequences=True))
  model.add(TimeDistributed(Dense(num_tokens, activation='softmax')))
  model.compile(optimizer='adam', loss='categorical_crossentropy',metrics=['accuracy'])
# summarize defined model
  model.summary()
  # plot_model(model, to_file='model.png', show_shapes=True)
  return model

# HIDDEN_UNITS=128

model = define_model()

!rm -rf weights/

if not os.path.exists('weights'):
    os.makedirs('weights')

WEIGHT_FILE_PATH = 'weights/qa-50-{epoch:03d}-{val_acc:.2f}.h5'

checkpoint = ModelCheckpoint(filepath=WEIGHT_FILE_PATH, save_best_only=True,monitor='val_acc', verbose=1)

model.fit(Xtrain, Ytrain, epochs=20, batch_size=BATCH_SIZE, validation_data=(Xtest, Ytest),callbacks=[checkpoint], verbose=1)

def decode_to_original_text(encoded):
  # integers = [argmax(vector) for vector in prediction]
  decoded = list()
  for i in encoded:
    word = idx2word[i]
    if word != '<unk>':
      decoded.append(word)
  return decoded

def predict_sequence(model, source):
  source = source.reshape((1, source.shape[0]))
  prediction = model.predict(source)[0]
  integers = [np.argmax(vector) for vector in prediction]
  target = decode_to_original_text(integers)
  return ' '.join(target),prediction,prediction.shape

Xtest[0].shape

raw_text=decode_to_original_text(Xtest[0])

raw_text

predict_sequence(model,Xtest[0])

scores = model.evaluate(Xtest,Ytest)

print("Accuracy is: %.2f%%" %(scores[1] * 100))

y_pred=model.predict(Xtest)

y_pred.shape

Xtest.shape

